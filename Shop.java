import java.util.Scanner;
public class Shop {

	public static void main(String[] args) {
		
		System.out.println("Hi customer! Welcome to Mathias' Furniture.");
		System.out.println("Here we give you the price of your future sofa.");
		System.out.println("The fabrics available are: Cotton, Linen, Velvet and Chenille. Also, the number of seats is in the range of 1-10");
		System.out.println("   ");
		
		Sofa[] sofa = new Sofa[4];
		
		Scanner sc = new Scanner(System.in);
		
		for(int i = 0; i < sofa.length; i++) {
			
			System.out.println("");
			System.out.println("Sofa number: " + (i + 1) + "  --------------");
			System.out.println("");
			
			sofa[i] = new Sofa();
			
			System.out.print("The fabric of your sofa is: ");
			sofa[i].fabric = sc.next();
			System.out.print("The number of seats is: ");
			sofa[i].nSeats = sc.nextInt();
			System.out.print("The style of the sofa is: ");
			sofa[i].style = sc.next();
			
		}
		
		System.out.println(" \n---------- Info & Price ----------\n");
		System.out.println("Last sofa fabric: " + sofa[3].fabric);
		System.out.println("Number of seats of last sofa: " + sofa[3].nSeats);
		System.out.println("Style of the last sofa: " + sofa[3].style);
		System.out.println("");
		
		sofa[3].sofaPrice(sofa[3].fabric,sofa[3].nSeats);
	}
 }
