public class Sofa {
	
	public String fabric;
	public int nSeats;
	public String style;
	
	public void sofaPrice(String fabric, int nSeats) {
		
		if(this.fabric.equals("Cotton") || this.fabric.equals("Linen")) {
			
			if(nSeats >= 5 && nSeats <= 10) {
				
				System.out.println("The price of the sofa is 1500 CAD.");
			}
			else if(nSeats >= 1 && nSeats <= 4) {
				
				System.out.println("The price of the sofa is 700 CAD.");
			}
			else {
			
			System.out.println("Sorry! An error has occurred, try again.");
			}
		}
		else if(this.fabric.equals("Velvet") || this.fabric.equals("Chenille")) {
			
			if(nSeats >= 3 && nSeats <= 10) {
				
				System.out.println("The price of the sofa is 500 CAD.");
			}
			else if(nSeats >= 1 && nSeats < 3) {
				
				System.out.println("The price of the sofa is 1700 CAD.");
			}
			else {
			
			System.out.println("Sorry! An error has occurred, try again.");
			}
		}
		else {
			
			System.out.println("Sorry! An error has occurred, try again.");
		}
	}
 }
 
 